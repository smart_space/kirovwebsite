﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

using Kirov.Data;

namespace Kirov.Authentication
{
    public class AuthenticationProvider
    {
        public User Authenticate(string loginName, string loginPassword, EFDbContext ctx)
        {
            if (loginName == "" || loginPassword == "")
            {
                return null;
            }

            List<User> users = ctx.Users.ToList();

            foreach (User user in users)
            {
                if (user.LoginName == loginName && user.LoginPassword == loginPassword)
                {
                    user.LoginSession = GetHash(loginName + loginPassword);
                    if (user.LoginSession != "")
                    {
                        ctx.SaveChanges();
                        return user;
                    }
                }
            }

            return null;
        }

        public void Unauthenticate(User user, EFDbContext ctx)
        {
            if (user == null)
            {
                return;
            }

            user.LoginSession = "";
            ctx.SaveChanges();
        }

        public User Authenticate(string loginSession, EFDbContext ctx)
        {
            if (loginSession == "")
            {
                return null;
            }

            List<User> users = ctx.Users.ToList();

            foreach (User user in users)
            {
                if (user.LoginSession == loginSession)
                {
                    return user;
                }
            }

            return null;
        }

        private string GetHash(string inputString)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (byte b in GetHashBytes(inputString))
                {
                    sb.Append(b.ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private byte[] GetHashBytes(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }
    }
}