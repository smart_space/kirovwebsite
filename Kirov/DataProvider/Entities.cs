﻿using System;

namespace Kirov.Data
{
    public class Order
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Comments { get; set; }
        public int Width { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public DateTime TimeCreated { get; set; }
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
        public int Price { get; set; }

        public Order()
        {
            Id = 0;
            Name = "";
            Email = "";
            Comments = "";
            Width = 0;
            Address = "";
            Status = 0;
            TimeCreated = DateTime.MinValue;
            ImageData = null;
            ImageMimeType = "";
            Price = 0;
        }
    }

    public class User
    {
        public int Id { get; set; }
        public string LoginName { get; set; }
        public string LoginPassword { get; set; }
        public string LoginSession { get; set; }
        public bool IsAdmin { get; set; }

        public User()
        {
            Id = 0;
            LoginName = "";
            LoginPassword = "";
            LoginSession = "";
            IsAdmin = false;
        }
    }

    public class PNKFloss
    {
        public int Id { get; set; }
        public int PNKId { get; set; }
        public string RGB { get; set; }
        public int LengthInMeters { get; set; }

        public PNKFloss()
        {
            Id = 0;
            PNKId = 0;
            RGB = "";
            LengthInMeters = 0;
        }
    }
}