﻿using System.Data.Entity;


namespace Kirov.Data
{
    public class EFDbContext : DbContext
    {
        public EFDbContext(string connectionString)
            : base(connectionString)
        {
            //Database.SetInitializer<EFDbContext>(new CreateDatabaseIfNotExists<EFDbContext>());
            Database.SetInitializer<EFDbContext>(null);
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<PNKFloss> PNKFlosses { get; set; }
    }
}
