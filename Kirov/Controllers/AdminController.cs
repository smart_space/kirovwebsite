﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kirov.Data;
using Kirov.Models;

namespace Kirov.Controllers
{
    public class AdminController : Controller
    {
        User AuthenticatedAdmin;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            using (EFDbContext ctx = new EFDbContext("EFDBContext"))
            {
                AuthenticatedAdmin = ControllerAuthenticationHelper.GetCurrentUser(this, ctx);

                if (AuthenticatedAdmin == null || AuthenticatedAdmin.IsAdmin == false)
                {
                    filterContext.Result = RedirectToAction("Login", "Account");
                }
            }    
        }

        public ActionResult Index()
        {
            EFDbContext ctx = new EFDbContext("EFDBContext");
            OrdersViewModel model = new OrdersViewModel(ctx.Orders.ToList());

            return View(model);
        }

        public FileContentResult GetImageForOrderWithId(int id)
        {
            using (EFDbContext ctx = new EFDbContext("EFDBContext"))
            {
                List<Order> orders = ctx.Orders.ToList();
                foreach (Order o in orders)
                {
                    if (o.Id == id)
                    {
                        return File(o.ImageData, o.ImageMimeType);
                    }
                }
            }

            return null;
        }

        public ActionResult ManageFloss()
        {
            EFDbContext ctx = new EFDbContext("EFDbContext");
            List<PNKFloss> floss = ctx.PNKFlosses.ToList();

            ManageFlossViewModel model = new ManageFlossViewModel(floss);
            return View(model);
        }

        [HttpPost]
        public RedirectToRouteResult UpdateOrderStatus(int id, int newStatus)
        {
            using (EFDbContext ctx = new EFDbContext("EFDBContext"))
            {
                List<Order> orders = ctx.Orders.ToList();
                foreach (Order o in orders)
                {
                    if (o.Id == id)
                    {
                        o.Status = newStatus;
                        ctx.SaveChanges();
                    }
                }
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public RedirectToRouteResult UpdateOrderPrice(int id, int newPrice)
        {
            using (EFDbContext ctx = new EFDbContext("EFDBContext"))
            {
                List<Order> orders = ctx.Orders.ToList();
                foreach (Order o in orders)
                {
                    if (o.Id == id)
                    {
                        o.Price = newPrice;
                        ctx.SaveChanges();
                    }
                }
            }

            return RedirectToAction("Index");
        }
    }
}