﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Net;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

using Kirov.Data;
using Kirov.Models;

namespace Kirov.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index(string alert = null)
        {
            ViewBag.Alert = alert;
            return View();
        }

        [HttpGet]
        public ActionResult Payment(int id)
        {
            EFDbContext ctx = new EFDbContext("EFDbContext");

            PaymentViewModel model = null;

            foreach (Order o in ctx.Orders)
            {
                if (o.Id == id)
                {
                    model = new PaymentViewModel(o);
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult NewOrder(string Name, string Email, HttpPostedFileBase File, int Width, string Comments, FormCollection collection)
        {
            if (Name == null || Name == "" || Email == null || Email == "" || File == null || Width == 0)
            {
                return new RedirectToRouteResult(
                    new RouteValueDictionary {
                        { "action", "Index" },
                        { "controller", "Home" },
                        { "alert", "Failure" }
                    }
                );
            }

            bool bRecapchaSuccess = false;

            string recaptchaResponse = collection["g-recaptcha-response"];

            if (recaptchaResponse != null && recaptchaResponse != "")
            {
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();
                    data["secret"] = "6LfsGR0UAAAAACDXZwd5xRA4KtirVu62t7HsJWdg";
                    data["response"] = recaptchaResponse;

                    var response = wb.UploadValues("https://www.google.com/recaptcha/api/siteverify", "POST", data);
                    string responseString = System.Text.Encoding.UTF8.GetString(response);
                    responseString = Regex.Replace(responseString, @"\s+", "");

                    if (responseString.Contains("\"success\":true"))
                    {
                        bRecapchaSuccess = true;
                    }
                }
            }
                
            if (!bRecapchaSuccess)
            {
                return new RedirectToRouteResult(
                    new RouteValueDictionary {
                        { "action", "Index" },
                        { "controller", "Home" },
                        { "alert", "RecapchaFailure" }
                    }
                );
            }

            Order newOrder = new Order();
            newOrder.Name = Name;
            newOrder.Email = Email;
            newOrder.Width = Width;
            newOrder.Comments = Comments;
            newOrder.TimeCreated = DateTime.Now;
            newOrder.ImageMimeType = File.ContentType;
            newOrder.ImageData = new byte[File.ContentLength];
            File.InputStream.Read(newOrder.ImageData, 0, File.ContentLength);

            EFDbContext ctx = new EFDbContext("EFDbContext");
            ctx.Orders.Add(newOrder);
            ctx.SaveChanges();

            return new RedirectToRouteResult(
                    new RouteValueDictionary {
                        { "action", "Index" },
                        { "controller", "Home" },
                        { "alert", "Success" }
                    }
                );
        }
    }
}