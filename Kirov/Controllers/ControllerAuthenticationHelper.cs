﻿using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kirov.Data;
using Kirov.Authentication;

namespace Kirov.Controllers
{
    public static class ControllerAuthenticationHelper
    {
        public static string SESSION_COOKIE_NAME = "PollyLoginSession";

        public static User GetCurrentUser(Controller controller, EFDbContext ctx)
        {
            AuthenticationProvider provider = new AuthenticationProvider();
            return provider.Authenticate(GetLoginSession(controller), ctx);
        }

        private static string GetLoginSession(Controller controller)
        {
            HttpCookie cookie = null;
            string loginSession = "";

            if (controller.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains(SESSION_COOKIE_NAME))
            {
                cookie = controller.ControllerContext.HttpContext.Request.Cookies[SESSION_COOKIE_NAME];
                loginSession = cookie.Value;
            }

            return loginSession;
        }
    }
}