﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kirov.Data;
using Kirov.Authentication;

namespace Kirov.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            HttpCookie cookie = null;
            string loginSession = "";

            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains(ControllerAuthenticationHelper.SESSION_COOKIE_NAME))
            {
                cookie = this.ControllerContext.HttpContext.Request.Cookies[ControllerAuthenticationHelper.SESSION_COOKIE_NAME];
                loginSession = cookie.Value;
            }

            EFDbContext ctx = new EFDbContext("EFDBContext");

            AuthenticationProvider provider = new AuthenticationProvider();
            User currentUser = provider.Authenticate(loginSession, ctx);
            if (currentUser != null)
            {
                provider.Unauthenticate(currentUser, ctx);

                cookie.Expires = DateTime.Now.AddDays(-1);
                this.ControllerContext.HttpContext.Response.Cookies.Set(cookie);
            }

            if (TempData["LoginErrorMessage"] != null)
            {
                ViewBag.LoginErrorMessage = TempData["LoginErrorMessage"];
                TempData.Remove("LoginErrorMessage");
            }

            return View();
        }

        [HttpPost]
        public RedirectToRouteResult Login(string loginName, string loginPassword, string admin)
        {
            EFDbContext ctx = new EFDbContext("EFDBContext");

            AuthenticationProvider provider = new AuthenticationProvider();
            User currentUser = provider.Authenticate(loginName, loginPassword, ctx);
            if (currentUser != null)
            {
                HttpCookie cookie = new HttpCookie(ControllerAuthenticationHelper.SESSION_COOKIE_NAME);
                cookie.Value = currentUser.LoginSession;
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);

                if (currentUser.IsAdmin && admin == "1")
                {
                    return RedirectToAction("Index", "Admin");
                }
                else if (!currentUser.IsAdmin && admin != "1")
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            TempData.Add("LoginErrorMessage", "Пожалуйста, проверьте Ваш логин и пароль и попробуйте еще раз.");

            return RedirectToAction("Login");
        }
    }
}