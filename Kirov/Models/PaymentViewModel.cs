﻿using Kirov.Data;

namespace Kirov.Models
{
    public class PaymentViewModel
    {
        public PaymentViewModel(Order order)
        {
            Order = order;
        }

        public Order Order { get; set; }
    }
}