﻿using System.Collections.Generic;

using Kirov.Data;

namespace Kirov.Models
{
    public class OrdersViewModel
    {
        public OrdersViewModel(List<Order> ords)
        {
            Orders = ords;
        }

        public List<Order> Orders { get; set; }
    }
}